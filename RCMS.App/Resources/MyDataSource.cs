﻿using System.Collections.ObjectModel;
using RCMS.Models;

namespace RCMS.App.Resources
{
    static class MyDataSource
    {
        private static ObservableCollection<Item> _allItems;
        private static User _loggedInUser;
        private static ObservableCollection<Category> _categories;
        private static ObservableCollection<User> _users;
        private static ObservableCollection<ProductUnit> _productUnits;
        public static ObservableCollection<Item> AllItems
        {
            get { return _allItems; }
            set { _allItems = value; }
        }

        public static User LoggedInUser
        {
            get { return _loggedInUser; }
            set { _loggedInUser = value; }
        }

        public static ObservableCollection<Category> Categories
        {
            get { return _categories; }
            set { _categories = value; }
        }

        public static ObservableCollection<User> Users
        {
            get { return _users; }
            set { _users = value; }
        }

        public static ObservableCollection<ProductUnit> ProductUnits
        {
            get { return _productUnits; }
            set { _productUnits = value; }
        }
    }
}
