﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using MahApps.Metro.Controls;
using RCMS.App.Resources;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Repositories;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private static  BackgroundWorker _backgroundWorker;
        public MainWindow()
        {
          
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(m_oWorker_DoWork);
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(m_oWorker_ProgressChanged);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(m_WorkerCompleted);
            _backgroundWorker.WorkerReportsProgress = true;
           
          
            InitializeComponent();
        }

        public  void m_WorkerCompleted(object sender, RunWorkerCompletedEventArgs runWorkerCompletedEventArgs)
        {
            if (runWorkerCompletedEventArgs.Cancelled)
            {

            }
          
        }

        public  void m_oWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
          
        }

        public  void m_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DbFactory dbFactory = new DbFactory();
            IItemRepository itemRepository = new ItemRepository(dbFactory);
            IItemService itemService = new ItemService(new UnitOfWork(dbFactory), itemRepository);
            MyDataSource.AllItems = new ObservableCollection<Item>(itemService.GetItems());
            _backgroundWorker.ReportProgress(100);
        }
    }
}
