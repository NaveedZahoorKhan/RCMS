﻿using System.Windows;
using System.Windows.Controls;
using Infragistics.Windows.Reporting;


namespace RCMS.App.Views
{
    /// <summary>
    /// Interaction logic for Billing
    /// </summary>
    public partial class Billing : UserControl
    {
        public Billing()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Report report = new Report();
            TextBlock block = new TextBlock();
            report.ReportSettings.PageSize = DesiredSize;
            TextBlock textBlock = new TextBlock();
            textBlock.Text = "Total Value:";
            ReportSection reportSection = new EmbeddedVisualReportSection(textBlock);
            block.FontSize = 30;
            block.Text = "RCMS";
            block.HorizontalAlignment = HorizontalAlignment.Center;
            block.VerticalAlignment = VerticalAlignment.Center;

            report.PageHeader = block;
            report.PageFooter = report.LogicalPageNumber;
            EmbeddedVisualReportSection section = new EmbeddedVisualReportSection(DataGrid);
            report.Sections.Add(section);
            
            //report.Print();
            XamReportPreview preview = new XamReportPreview();
            preview.GeneratePreview(report, false, true);

            Window window = new Window
            {
                Content = preview,
                ShowActivated = true
            };
            window.ShowDialog();
        }
     
    }
}
