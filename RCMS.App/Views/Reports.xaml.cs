﻿using System.Windows;
using System.Windows.Controls;
using Infragistics.Windows.Reporting;

namespace RCMS.App.Views
{
    /// <summary>
    /// Interaction logic for Reports
    /// </summary>
    public partial class Reports : UserControl
    {
        public Reports()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Report report = new Report();
            TextBlock block = new TextBlock
            {
                Text = "RCMS",
                FontSize = 16
            };

            StackPanel stackPanel = new StackPanel { Orientation = Orientation.Vertical };
            stackPanel.Children.Add(block);
            Separator separator = new Separator { Height = 140 };
            stackPanel.Children.Add(separator);

            //            stackPanel.Children.Add(new Label() {Height=240});
            EmbeddedVisualReportSection section = new EmbeddedVisualReportSection(DataGrid);
            EmbeddedVisualReportSection section1 = new EmbeddedVisualReportSection(stackPanel);

            report.Sections.Add(section);
            //report.Print();
            XamReportPreview preview = new XamReportPreview();
            preview.GeneratePreview(report, false, true);

            Window window = new Window
            {
                Content = preview,
                ShowActivated = true
            };
            window.ShowDialog();
        }
    }
}
