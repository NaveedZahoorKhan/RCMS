﻿using System.Windows;
using System.Windows.Controls;
using Infragistics.Windows.DataPresenter;
using Infragistics.Windows.Reporting;

namespace RCMS.App.Views
{
    /// <summary>
    /// Interaction logic for Sales.xaml
    /// </summary>
    public partial class Sales : UserControl
    {
        public Sales()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Report report = new Report();
            TextBlock block = new TextBlock();
            block.Text = "RCMS";
            block.FontSize = 16;

            StackPanel stackPanel = new StackPanel {Orientation = Orientation.Vertical};
            stackPanel.Children.Add(block);
            Separator separator = new Separator {Height = 140};
            stackPanel.Children.Add(separator);
            
//            stackPanel.Children.Add(new Label() {Height=240});
            EmbeddedVisualReportSection section = new EmbeddedVisualReportSection(DataGrid);
            EmbeddedVisualReportSection section1 = new EmbeddedVisualReportSection(stackPanel);

            report.Sections.Add(section1);
            report.Sections.Add(section);
            //report.Print();
            XamReportPreview preview = new XamReportPreview();
            preview.GeneratePreview(report, false, true);

            Window window = new Window();
            window.Content = preview;
            window.ShowActivated = true;
            window.ShowDialog();
        }

        private void Sales_OnLoaded(object sender, RoutedEventArgs e)
        {
            DataGrid.FieldSettings.SummaryDisplayArea = SummaryDisplayAreas.Bottom;
            FieldLayout fieldLayout = DataGrid.FieldLayouts[0];
            fieldLayout.SummaryDescriptionMask = "Totals";
            fieldLayout.SummaryDescriptionMaskInGroupBy = "Summaries for [GROUP_BY_VALUE]";

            fieldLayout.Settings.SummaryDescriptionVisibility = Visibility.Visible;
            fieldLayout.SummaryDefinitions.Add(SummaryCalculator.Sum, "HeadIn");

        }
    }
}
