﻿using System;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using RCMS.App.Resources;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App.Views
{
    /// <summary>
    /// Interaction logic for Management.xaml
    /// </summary>
    public partial class Management : UserControl
    {
        public Management()
        {
            InitializeComponent();
        }

        private Item _item;
        private Category _category;

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                IDbFactory dbFactory = new DbFactory();
                IItemRepository itemRepository = new ItemRepository(dbFactory);
                IItemService itemService = new ItemService(new UnitOfWork(dbFactory), itemRepository);
                MyDataSource.AllItems.Remove(_item);
                var item = itemService.GetItem(_item.Id);
                itemService.Delete(item);
                itemService.SaveItem();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                
            }
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _item = ProductDataGrid.CurrentItem as Item;
        }

        private void ButtonCategoryDelete(object sender, RoutedEventArgs e)
        {
            IDbFactory dbFactory = new DbFactory();
            ICategoryRepository categoryRepository = new CategoryRepository(dbFactory);
            ICategoryService categoryService = new CategoryService(new UnitOfWork(dbFactory), categoryRepository);
            if (_category == null) return;
            if (_category.Id < 0) return;

            var categoryToDelete = categoryService.GetCategory(_category.Id);
            try
            {
                MyDataSource.Categories.Remove(categoryToDelete);
                categoryService.Delete(categoryToDelete);
                categoryService.SaveCategory();
            }
            catch (Exception exception)
            {
                TextBlock block = new TextBlock();

                block.Text = "An Error Occurred while trying to delete";
                block.FontSize = 16;
                StackPanel panel = new StackPanel();
                panel.Children.Add(block);
                panel.HorizontalAlignment = HorizontalAlignment.Center;
                panel.VerticalAlignment = VerticalAlignment.Center;
                MetroWindow window = new MetroWindow();
                window.ShowActivated = true;
                window.Height = 190;
                window.Width = block.Width;
                window.Content = panel;
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

                window.ShowDialog();
            }
        }

        private void CategoryGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _category = CategoryGrid.CurrentItem as Category;

        }

        private User _user;

        private void UserDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _user = UserDataGrid.CurrentItem as User;
        }

        private void UserDataGridDelete(object sender, RoutedEventArgs e)
        {
            IDbFactory dbFactory = new DbFactory();
            IUserRepository userRepository = new UserRepository(dbFactory);
            IUserService userService = new UserService(new UnitOfWork(dbFactory), userRepository);
            try
            {
                var userToDelete = userService.GetUserById(_user.Id);

                userService.Delete(userToDelete);

                userService.SaveUser();
                MyDataSource.Users.Remove(userToDelete);
            }
            catch (Exception)
            {

                TextBlock block = new TextBlock
                {
                    Text = "An Error Occurred while trying to delete",
                    FontSize = 16
                };

                var panel = new StackPanel();
                panel.Children.Add(block);
                panel.HorizontalAlignment = HorizontalAlignment.Center;
                panel.VerticalAlignment = VerticalAlignment.Center;
                MetroWindow window = new MetroWindow
                {
                    ShowActivated = true,
                    Height = 190,
                    Width = block.Width,
                    Content = panel,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };

                window.ShowDialog();
            }

        }

        private ProductUnit _productUnit;

        private void ProductUnitDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _productUnit = ProductUnitDataGrid.CurrentItem as ProductUnit;
        }

        private void ProductUnitDataGridDelete(object sender, RoutedEventArgs e)
        {
            IDbFactory dbFactory = new DbFactory();
            IProductUnitsRepository productUnitsRepository = new ProductUnitRepository(dbFactory);
            IProductUnitService productUnitService = new ProductUnitService(new UnitOfWork(dbFactory),productUnitsRepository);
            try
            {
                var productUnitToDelete = productUnitService.GetProductUnit(_productUnit.Id);
                productUnitService.Delete(productUnitToDelete);
                productUnitService.SaveProductUnit();
              
            }
            catch (Exception)
            {

                TextBlock block = new TextBlock
                {
                    Text = "An Error Occurred while trying to delete",
                    FontSize = 16
                };

                var panel = new StackPanel();
                panel.Children.Add(block);
                panel.HorizontalAlignment = HorizontalAlignment.Center;
                panel.VerticalAlignment = VerticalAlignment.Center;
                MetroWindow window = new MetroWindow
                {
                    ShowActivated = true,
                    Height = 190,
                    Width = block.Width,
                    Content = panel,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                };

                window.ShowDialog();
            }

        }

        private void ProductSaveClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Product Save", "Product Save Successfully", MessageBoxButton.OK);
            PType.SelectedIndex = 0;
            PUnit.SelectedIndex = 0;
            PCategory.SelectedIndex = 0;
        }
    }
}
