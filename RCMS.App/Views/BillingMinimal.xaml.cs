﻿using System.Windows.Controls;

namespace RCMS.App.Views
{
    /// <summary>
    /// Interaction logic for BillingMinimal
    /// </summary>
    public partial class BillingMinimal : UserControl
    {
        public BillingMinimal()
        {
            InitializeComponent();
           var billing = new RCMS.App.Reports.Billing();
            CrystalReportsViewer.DataContext = billing;
        }

       
     
    }
}
