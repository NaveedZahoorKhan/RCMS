﻿using System.Windows;
using Prism.Mvvm;
using Unity;

namespace RCMS.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            IUnityContainer container = new UnityContainer();
            ViewModelLocationProvider.SetDefaultViewModelFactory((type) => container.Resolve(type));
            var bootstrapper = new Bootstrapper();
            bootstrapper.Run();
        }
    }
}
