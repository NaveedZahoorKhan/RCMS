﻿using System.Collections.ObjectModel;
using Prism.Unity;
using RCMS.App.Views;
using System.Windows;
using Microsoft.Practices.Unity;
using Prism.Regions;
using RCMS.App.Resources;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App
{
    class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }


        private RegionManager _regionManager;

        protected override void InitializeShell()
        {
            Window window = new Login();
            window.ShowActivated = true;
            var res = window.ShowDialog();


//          Window splash= new CustomSplashScreen();
//            splash.ShowActivated = true;
//            splash.ShowDialog();

            if (Application.Current.MainWindow != null) Application.Current.MainWindow.Show();

            #region RegionManager
            _regionManager = new RegionManager();
            _regionManager.RegisterViewWithRegion("MainRegion", typeof(Home));

            Container.RegisterTypeForNavigation<Billing>();
            Container.RegisterTypeForNavigation<Views.Reports>();
            Container.RegisterTypeForNavigation<Views.BillingMinimal>();
            Container.RegisterTypeForNavigation<Sales>();
            Container.RegisterTypeForNavigation<Management>();
            Container.RegisterTypeForNavigation<Home>();

            #endregion
            //                        Container.RegisterTypeForNavigation<Orders>();
            //                        Container.RegisterTypeForNavigation<Purchases>();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterType<IDbFactory, DbFactory>();
            Container.RegisterType<IUnitOfWork, UnitOfWork>();
            Container.RegisterType<IInvoiceMasterRepository, InvoiceMasterRepository>();
            Container.RegisterType<IInvoiceMasterService, InvoiceMasterService>();
            Container.RegisterType<IItemRepository, ItemRepository>();
            Container.RegisterType<IItemService, ItemService>();
            Container.RegisterType<ICategoryRepository, CategoryRepository>();
            Container.RegisterType<ICategoryService, CategoryService>();
            Container.RegisterType<IUserRepository, UserRepository>();
            Container.RegisterType<IUserService, UserService>();
            Container.RegisterType<IProductUnitsRepository, ProductUnitRepository>();
            Container.RegisterType<IProductUnitService, ProductUnitService>();


            #region Instances

            Container.RegisterInstance(new DbFactory());
            Container.RegisterInstance(new UnitOfWork(Container.Resolve<IDbFactory>()));
            Container.RegisterInstance(new InvoiceMasterRepository(Container.Resolve<IDbFactory>()));
            Container.RegisterInstance(new InvoiceMasterService(Container.Resolve<IUnitOfWork>(),
                Container.Resolve<IInvoiceMasterRepository>()));
            Container.RegisterInstance(new ItemRepository(Container.Resolve<IDbFactory>()));
            Container.RegisterInstance(new ItemService(Container.Resolve<IUnitOfWork>(),
                Container.Resolve<IItemRepository>()));
            Container.RegisterInstance(new CategoryRepository(Container.Resolve<IDbFactory>()));
            Container.RegisterInstance(new CategoryService(Container.Resolve<IUnitOfWork>(),
                Container.Resolve<CategoryRepository>()));
            Container.RegisterInstance(new UserRepository(Container.Resolve<IDbFactory>()));
            Container.RegisterInstance(new UserService(Container.Resolve<IUnitOfWork>(),
                Container.Resolve<UserRepository>()));
            Container.RegisterInstance(new ProductUnitRepository(Container.Resolve<IDbFactory>()));
            Container.RegisterInstance(new ProductUnitService(Container.Resolve<IUnitOfWork>(),
                Container.Resolve<ProductUnitRepository>()));

            #endregion
        }

        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();
        }
    }
}