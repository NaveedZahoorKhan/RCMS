﻿using Prism.Mvvm;
using System.Collections.Generic;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App.ViewModels
{
    public class ReportsViewModel : BindableBase
    {
        private IEnumerable<InvoiceMaster> _invoiceMasters;

        public IEnumerable<InvoiceMaster> InvoiceMasters
        {
            get { return _invoiceMasters; }
            set { SetProperty(ref _invoiceMasters, value); }
        }

        public ReportsViewModel(IInvoiceMasterService masterService)
        {
            InvoiceMasters = masterService.GetInvoiceMasters();
        }
    }
}
