﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using RCMS.App.Resources;
using RCMS.Commons.HelperClasses;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App.ViewModels
{
    public class LoginViewModel : BindableBase
    {
        #region private fields
        
        private readonly Navigation _navigation;
        private string _username;
        private string _password;
        private string _passwordText;
        #endregion

        public string UserName
        {
            get
            {
                return _username;
            }
            set
            {
                SetProperty(ref _username, value);
                RaisePropertyChanged("UserName");
                CanAttemptLogin("abs");
            }
        }

        private User User { get; set; }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                SetProperty(ref _password, "*");
                SetProperty(ref _passwordText, value);
            }
        }

        public bool ErrorVisibility
        {
            get { return _errorVisibility; }
            set { SetProperty(ref _errorVisibility, value); }
        }

        public DelegateCommand<object> Home { get; private set; }
        public IRegionManager RegionManager;
        private readonly IUserService _userservice;
        private bool _errorVisibility = false;

        public LoginViewModel(IRegionManager regionManager, IUserService userservice)
        {

            UserName = "Admin";
           
            _userservice = userservice;
            _navigation = new Navigation(regionManager);
            Cancel = new DelegateCommand(() => Environment.Exit(0));
            Home = new DelegateCommand<object>(AttemptLogin );
        }
        public DelegateCommand Cancel { get; set; }

        public string PasswordText
        {
            get { return _passwordText; }
            set { _passwordText = value; }
        }

        public bool CanAttemptLogin(string arg)
        {
            return !string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password);
        }

        private void AttemptLogin(object path)
        {

            var passwordBox = path as PasswordBox;
            if (passwordBox != null) PasswordText = passwordBox.Password;
            var user = new User
            {
                Name = UserName,
                Password = PasswordText
            };
            User = _userservice.UserLogin(user);
            if (User == null)
            {
                ErrorVisibility = true;
            }
            else if(User.IsActive == true)
            {
                MyDataSource.LoggedInUser = User;
                var wind = Application.Current.Windows.OfType<Window>().SingleOrDefault(window => window.IsActive);
                wind?.Close();
            }

        }
            
    }
}
