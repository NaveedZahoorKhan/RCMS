﻿using System;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Prism.Mvvm;
using System.Windows;
using RCMS.App.Resources;
using RCMS.App.Views;
using RCMS.Models;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App.ViewModels
{
    public class CustomSplashScreenViewModel : BindableBase
    {
        private string _loadingScreen;
        private long _progressbar;
        public string LoadingScreen { get { return _loadingScreen; } set { SetProperty(ref _loadingScreen, value); } }
        public long ProgressBar { get { return _progressbar; } set { SetProperty(ref _progressbar, value); } }
        public CustomSplashScreenViewModel(IItemService itemService, ICategoryService categoryService, UserService userService, ProductUnitService productUnitService)
        {
            Task task = new Task(() =>
            {
                LoadingScreen += "Loading Products......\n";
                ProgressBar = 50;
                Thread.Sleep(2000);
                MyDataSource.AllItems = new ObservableCollection<Item>(itemService.GetMany(item => item.Qty > 0));
                LoadingScreen += "Loading Categories......\n";
                ProgressBar = 60;
                Thread.Sleep(2000);


                MyDataSource.Categories = new ObservableCollection<Category>(categoryService.GetCategorys());
                LoadingScreen += "Loading Users.......\n";
                ProgressBar = 70;
                Thread.Sleep(2000);

                MyDataSource.Users = new ObservableCollection<User>(userService.GetAllUser());
                MyDataSource.LoggedInUser = MyDataSource.Users[0];
                LoadingScreen += "Loading Product Units........\n";
                ProgressBar = 100;
                Thread.Sleep(2000);

                MyDataSource.ProductUnits = new ObservableCollection<ProductUnit>(productUnitService.GetProductUnits());
                Dispatcher.CurrentDispatcher.Invoke(new Action(CloseThisWindow), DispatcherPriority.Background);
            });
            task.Start(TaskScheduler.Current);
      
              
            
        }

        private void CloseThisWindow()
        {
            var v = Application.Current.Windows[0];
            v?.Hide();
        }
    }
}