﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Microsoft.Win32;
using RCMS.App.Resources;
using RCMS.Commons;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App.ViewModels
{
    public class ManagementViewModel : BindableBase, IDataErrorInfo
    {
        #region Private Fields

        #region User

        private string _username;
        private string _useraddress;
        private string _userimg = "../Resources/Icons/user.png";
        private string _designation;
        private string _city = null;
        private string _userCell;
        private string _userPhone;
        private string _password;
        public DelegateCommand UserBrowse { get; private set; }

        #endregion

        #region Product Unit

        private string _pUUnit;
        private string _pUName;
        private IEnumerable<ProductUnit> _pUnit;

        #endregion

        #region Category

        private string _name;
        private string _discription;
        private string _img;
        private string _lowerlimit;
        private IEnumerable<Category> _categorylist;
        private ICategoryService _categoryService;

        #endregion

        #region Product

        private IEnumerable<Category> _productCategory;
        private ProductUnit _selectedProductUnit;

        private string _productname;
        private string _productcode;
        private string _productdiscount;
        private string _price;
        private string _productImg;
        private string _productqty;
        private IEnumerable<ProductUnit> _productunit;
        private IEnumerable<Item> _itemlist;

        #endregion

        private bool _ringVisibility;

        private Category _selectedProductCategory;

        private static readonly Random Random = new Random();

        #endregion



        #region Properties

        #region Categories

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public string Discription
        {
            get { return _discription; }
            set { SetProperty(ref _discription, value); }
        }

        public string Img
        {
            get { return _img; }
            set { SetProperty(ref _img, value); }
        }

        public string LowerLimit
        {
            get { return _lowerlimit; }
            set { SetProperty(ref _lowerlimit, value); }
        }

        public IEnumerable<Category> CategoryList
        {
            get { return _categorylist; }
            set { SetProperty(ref _categorylist, value); }
        }

        public ObservableCollection<int> CategoryOrder
        {
            get { return _categoryOrder; }
            set { _categoryOrder = value; }
        }

        #endregion

        #region Products

        public string ProductName
        {
            get { return _productname; }
            set { SetProperty(ref _productname, value); }
        }

        public string ProductCode
        {
            get { return _productcode; }
            set { SetProperty(ref _productcode, value.ToUpper()); }
        }

        public string ProductPrice
        {
            get { return _price; }
            set { SetProperty(ref _price, value); }
        }

        public string ProductDiscount
        {
            get { return _productdiscount; }
            set { SetProperty(ref _productdiscount, value); }
        }

        public IEnumerable<Category> ProductCategory
        {
            get { return _productCategory; }
            set { SetProperty(ref _productCategory, value); }
        }

        public IEnumerable<ProductUnit> ProductUnit
        {
            get { return _productunit; }
            set { SetProperty(ref _productunit, value); }
        }

        public DelegateCommand ProductSave { get; private set; }

        public string ProductImg
        {
            get { return _productImg; }
            set { SetProperty(ref _productImg, value); }
        }

        public DelegateCommand ProductBrowse { get; private set; }

        #endregion

        #endregion

        #region Methods

        #region Category

        private void SaveCategory()
        {
            Category category = new Category
            {
                Name = Name,
                CategoryImage = Img,
                Description = Discription,
                ItemLowerLimit = LowerLimit,
                DateCreated = DateTime.Now,
                CategoryOrder = SelectedCategoryOrder



            };
            _categoryService.CreateCategory(category);
            _categoryService.SaveCategory();
            _categoryService.RefreshEntity(category);
            InitVals();
            MyDataSource.Categories.Add(category);
        }


        #endregion

        #region Others

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        private IProductUnitService _productUnitService;
        private ItemService _itemserv;
        private UserService _userService;
        private AddressesService _addressesService;
        private ObservableCollection<int> _categoryOrder;
        public DelegateCommand SaveUser { get; private set; }
        private int _selectedCategoryOrder;
        private ObservableCollection<User> _user;



        private ObservableCollection<Item> _itemsList;
        public DelegateCommand<object> DeleteItem { get; private set; }
        public DelegateCommand EditProductSave { get; private set; }
        public DelegateCommand EditProductBrowse { get; private set; }

        public string EditProductImg
        {
            get { return _editProductImg; }
            set { SetProperty(ref _editProductImg, value); }
        }

        public ManagementViewModel()
        {
            SaveUser = new DelegateCommand(SaveUserData);
            UserBrowse = new DelegateCommand(() => UserImg = RaiseBrowseWindow());
            // DeleteItem = new DelegateCommand<object>(DeleteThisItem);
            IDbFactory dbFactory = new DbFactory();
            IProductUnitsRepository repository = new ProductUnitRepository(dbFactory);
            ICategoryRepository catrepo = new CategoryRepository(dbFactory);
            IUserRepository userRepository = new UserRepository(dbFactory);
            IAddressesRepository addressesRepository = new AddressesRepository(dbFactory);
            _addressesService = new AddressesService(new UnitOfWork(dbFactory), addressesRepository);
            IItemRepository itemrepo = new ItemRepository(dbFactory);
            _itemserv = new ItemService(new UnitOfWork(dbFactory), itemrepo);
            _userService = new UserService(new UnitOfWork(dbFactory), userRepository);
            ItemsList = MyDataSource.AllItems;
            Users = MyDataSource.Users;
            EditProductBrowse = new DelegateCommand(() => EditProductImg = RaiseBrowseWindow());

            _itemserv = new ItemService(new UnitOfWork(dbFactory), itemrepo);
            _categoryService = new CategoryService(new UnitOfWork(dbFactory), catrepo);
            _productUnitService = new ProductUnitService(new UnitOfWork(dbFactory), repository);
            ProductSave = new DelegateCommand(SaveProduct);
            Save = new DelegateCommand(SaveCategory);
            CategoryList = MyDataSource.Categories;
            EditProductSave = new DelegateCommand(UpdateProduct);

            var list = new List<Category>(CategoryList);

            CategoryOrder = new ObservableCollection<int> {1, 2, 3, 4, 5};
            for (var i = 0; i < list.Count; i++)
            {
                if (list.Exists(category => category.CategoryOrder == i))
                {
                    CategoryOrder.Remove(i);
                }
            }

            Browse = new DelegateCommand(() => Img = RaiseBrowseWindow());
            ProductBrowse = new DelegateCommand(() => ProductImg = RaiseBrowseWindow());

            ItemList = _itemserv.GetItems();
            GetProductUnit();
            PUSave = new DelegateCommand(SaveProductUnit);
            PUnit = MyDataSource.ProductUnits;
            ProductCategory = CategoryList;
//            new Thread( () => ProductCategory = _unitOfWork.Repository<Category>().GetAll().ToList()).Start();
        }


        public ObservableCollection<User> Users
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private void SaveUserData()
        {

            var addresses = new Addresses
            {
                City = City,
                Country = "Pakistan",
                AddressLine1 = UserAddress
            };

            var user = new User
            {
                Addresses = addresses,
                Designation = Designation,
                IsActive = true,
                Name = UserName,
                Password = Password,
                UserImg = UserImg,
                CreatedBy = MyDataSource.LoggedInUser.Id,
                DateCreated = DateTime.Now
            };
            _userService.CreateUser(user);
            _addressesService.CreateAddresses(addresses);
            _addressesService.SaveAddresses();
            _userService.SaveUser();
            MyDataSource.Users.Add(user);
            UserAddress = string.Empty;
            City = string.Empty;
            Designation = string.Empty;
            UserImg = string.Empty;

            
        }

        public void InitializeCollectionView()
        {
            var units = _productUnitService.GetProductUnits();
            var u = new ObservableCollection<ProductUnit>(units);
            ProductUnit = new BindingList<ProductUnit>(u);
        }

        private void GetProductUnit()
        {
            InitializeCollectionView();
            RingVisibility = false;
        }

        private void SaveProductUnit()
        {
            Models.ProductUnit productUnit = new ProductUnit();
            productUnit.Unit = PUUnit;
            productUnit.UnitFullName = PUName;
            _productUnitService.Add(productUnit);
            _productUnitService.SaveProductUnit();
            PUUnit = null;
            PUName = null;
            MyDataSource.ProductUnits.Add(productUnit);
        }

        private void SaveProduct()
        {
            Item item = new Item
            {
                Code = ProductCode,
                Category = SelectedProductCategory,
                ProductUnit = SelectedProductUnit,
                Discount = double.Parse(ProductDiscount),
                Name = ProductName,
                Img = ProductImg,
                Type = SelectedProductType,
                Price = double.Parse(ProductPrice),
                Qty = int.Parse(ProductQty)
                
            };
            item.Type = SelectedProductType;
            _itemserv.CreateItem(item);
            _itemserv.SaveItem();
            MyDataSource.AllItems.Add(item);
            ProductCode = String.Empty;
            ProductDiscount = String.Empty;
            ProductName = string.Empty;
            ProductImg = string.Empty;
            ProductPrice = string.Empty;
            

        }

        private void BindToControls()
        {
            var item = SelectedProductForEdit;
            EditProductCode = item.Code;
            EditSelectedProductCategory = item.Category;
            EditSelectedProductUnit = item.ProductUnit;
            EditProductDiscount = item.Discount.ToString();
            EditProductName = item.Name;
            EditProductPrice = item.Price.ToString();
            EditSelectedProductType = item.Type;
            EditProductQty = item.Qty.ToString();

        }

        public ProductType EditSelectedProductType
        {
            get { return _editSelectedProductType; }
            set { SetProperty(ref _editSelectedProductType, value); }
        }

        private void UpdateProduct()
        {
            var itemToRemove = _itemserv.GetItem(SelectedProductForEdit.Id);

            Item item = new Item
            {
                Code = EditProductCode,
                Category = EditSelectedProductCategory,
                ProductUnit = EditSelectedProductUnit,
                Discount = double.Parse(EditProductDiscount),
                Name = EditProductName,



                Type = EditSelectedProductType,
                Price = double.Parse(EditProductPrice),
                Qty = int.Parse(EditProductQty),
                Img = EditProductImg
            };
            _itemserv.UpdateItem(item);
            _itemserv.SaveItem();

            MyDataSource.AllItems.Remove(itemToRemove);
            MyDataSource.AllItems.Add(item);

        }

        public string EditProductQty
        {
            get { return _editProductQty; }
            set { SetProperty(ref _editProductQty, value); }
        }

        public string EditProductPrice
        {
            get { return _editProductPrice; }
            set { SetProperty(ref _editProductPrice, value); }
        }

        public string EditProductName
        {
            get { return _editProductName; }
            set { SetProperty(ref _editProductName, value); }
        }

        public string EditProductDiscount
        {
            get { return _editProductDiscount; }
            set { SetProperty(ref _editProductDiscount, value); }
        }

        public ProductUnit EditSelectedProductUnit
        {
            get { return _editSelectedProductUnit; }
            set { SetProperty(ref _editSelectedProductUnit, value); }
        }

        public Category EditSelectedProductCategory
        {
            get { return _editSelectedProductCategory; }
            set { SetProperty(ref _editSelectedProductCategory, value); }
        }

        public string EditProductCode
        {
            get { return _editProductCode; }
            set { SetProperty(ref _editProductCode, value); }
        }

        private string RaiseBrowseWindow()
        {
            FileDialog dialog = new OpenFileDialog();
            dialog.Title = "Please select an Image";
            dialog.Filter = "Image Files | *.jpg;*.png;*.jpeg;";
            var res = dialog.ShowDialog();
            if (!res.Equals(true)) return null;
            return dialog.FileName;
        }

        private ProductType _productType;

        private void InitVals()
        {
            Name = null;
            Img = null;
            Discription = null;
            LowerLimit = null;
        }

        #endregion

        #endregion

        public DelegateCommand PUSave { get; private set; }

        public DelegateCommand Browse { get; private set; }

        public DelegateCommand Save { get; private set; }

        public bool RingVisibility
        {
            get { return _ringVisibility; }
            set { _ringVisibility = value; }
        }

        public IEnumerable<Item> ItemList
        {
            get { return _itemlist; }
            set { _itemlist = value; }
        }

        public string UserName
        {
            get { return _username; }
            set { _username = value; }
        }

        public string UserAddress
        {
            get { return _useraddress; }
            set { _useraddress = value; }
        }

        public string UserImg
        {
            get { return _userimg; }
            set { SetProperty(ref _userimg, value); }
        }

        public string Designation
        {
            get { return _designation; }
            set { _designation = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string UserCell
        {
            get { return _userCell; }
            set { _userCell = value; }
        }

        public string UserPhone
        {
            get { return _userPhone; }
            set { _userPhone = value; }
        }

        public ProductUnit SelectedProductUnit
        {
            get { return _selectedProductUnit; }
            set { SetProperty(ref _selectedProductUnit, value); }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public Category SelectedProductCategory
        {
            get { return _selectedProductCategory; }
            set { _selectedProductCategory = value; }
        }

        public string PUUnit
        {
            get { return _pUUnit; }
            set { _pUUnit = value; }
        }

        public string PUName
        {
            get { return _pUName; }
            set { _pUName = value; }
        }

        public IEnumerable<ProductUnit> PUnit
        {
            get { return _pUnit; }
            set
            {
                SetProperty(ref _pUnit, value);
                RaisePropertyChanged("PUnit");
            }
        }

        public int SelectedCategoryOrder
        {
            get { return _selectedCategoryOrder; }
            set { _selectedCategoryOrder = value; }
        }

        public ObservableCollection<Item> ItemsList
        {
            get { return _itemsList; }

            set { SetProperty(ref _itemsList, value); }
        }

        public string ProductQty
        {
            get { return _productqty; }
            set { _productqty = value; }
        }

        private ProductType _selectedProductType;
        private Item _selectedProductForEdit;
        private string _editProductCode;
        private Category _editSelectedProductCategory;
        private ProductUnit _editSelectedProductUnit;
        private string _editProductDiscount;
        private string _editProductName;
        private string _editProductPrice;
        private string _editProductQty;
        private string _editProductImg;
        private ProductType _editSelectedProductType;
        private bool _productSaveButton;

        public IEnumerable<ProductType> ProductType
        {
            get { return Enum.GetValues(typeof(ProductType)).Cast<ProductType>(); }
            set
            {
                foreach (var productType in value)
                    SetProperty<ProductType>(ref _productType, productType);
            }
        }

        public Item SelectedProductForEdit
        {
            get { return _selectedProductForEdit; }
            set
            {
                SetProperty(ref _selectedProductForEdit, value);
                BindToControls();
            }
        }

        public ProductType SelectedProductType
        {
            get { return _selectedProductType; }
            set { SetProperty(ref _selectedProductType, _selectedProductType); }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "ProductName" && !string.IsNullOrEmpty(ProductName))
                {
                    if (this.ProductName.Length > 20)
                    {
                        ProductSaveButton = false;
                        return "Product Name Can't be greater than 20";

                    }
                }
                if (columnName == "ProductCode" && !string.IsNullOrEmpty(ProductCode))
                {
                    if (this.ProductCode.Length > 4)
                    {
                        ProductSaveButton = false;
                        return "Product Code length is > 4";
                    }
                }
                if (!string.IsNullOrEmpty(ProductPrice) && columnName == "ProductPrice")
                {
                    if (int.Parse(ProductPrice) > 1000)
                    {
                        ProductSaveButton = false;
                        return "Price can't be more than 1000";
                    }
                }
                if (!string.IsNullOrEmpty(ProductDiscount) && columnName == "ProductDiscount")
                {
                    if (int.Parse(ProductDiscount) > 100)
                    {
                        ProductSaveButton = false;
                        return "Discount Can't Be More than 100";
                    }
                }
                if (!string.IsNullOrEmpty(ProductQty) && columnName == "ProductQty")
                {
                    if (int.Parse(ProductQty) > 50)
                    {
                        ProductSaveButton = false;
                        return "Product Qty can't be more than 50";
                    }
                }
                if (columnName== "ProductCategoryOrder" &&!(SelectedCategoryOrder>= 0))
                {
                    return "You should selected one order";
                }
                if (!string.IsNullOrEmpty(ProductName) )
                {
                    ProductSaveButton = true;
                }
                return String.Empty;
            }
        }

        public string Error
        {
            get { return string.Empty; }

        }

        public bool ProductSaveButton
        {
            get { return _productSaveButton; }
            set { SetProperty(ref _productSaveButton, value); }
        }
    }
}