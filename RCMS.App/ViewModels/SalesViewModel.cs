﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Commands;
using Prism.Mvvm;
using RCMS.App.Resources;
using RCMS.Commons;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App.ViewModels
{
    public class SalesViewModel : BindableBase
    {

        #region Private fields

        private IEnumerable<Item> _products;


        #endregion

        #region Properties

        public IEnumerable<Item> Products
        {
            get { return _products; }
            set { _products = value; }
        }

        public string Category1
        {
            get { return _category1; }
            set { _category1 = value; }
        }
        public string Categor2
        {
            get { return _category2; }
            set { _category2 = value; }
        }
        public Category Cat1Category
        {
            get { return _cat1Category; }
            set { _cat1Category = value; }
        }

        #endregion

        #region Methods



        #endregion

        private IItemService _itemService;
        private ICategoryService _categoryService;
        private string _category1;
        private Category _cat1Category;
        private string _category2;
        private ObservableCollection<Item> _listView1;
        private Item _selectedItems;
        private ObservableCollection<Item> _items;
        private IInvoiceDetailService _invoiceDetailService;
        private IInvoiceMasterService _invoiceMasterService;
        public DelegateCommand Cencel { get; private set; }

        public string VendorName
        {
            get { return _vendorName; }
            set { SetProperty(ref _vendorName, value); }
        }

        public SalesViewModel()
        {
            IDbFactory dbFactory = new DbFactory();
            IItemRepository itemRepository = new ItemRepository(dbFactory);
            ICategoryRepository categoryrepo = new CategoryRepository(dbFactory);
            IInvoiceMasterRepository masterrepo = new InvoiceMasterRepository(dbFactory);
            IInvoiceDetailRepository detailrepo = new InvoiceDetailRepository(dbFactory);
            _itemService = new ItemService(new UnitOfWork(dbFactory), itemRepository);
            _categoryService = new CategoryService(new UnitOfWork(dbFactory), categoryrepo);
            _invoiceMasterService = new InvoiceMasterService(new UnitOfWork(dbFactory), masterrepo);
            _invoiceDetailService = new InvoiceDetailService(new UnitOfWork(dbFactory), detailrepo);
            Cat1Category = _categoryService.GetCategoryByCategoryOrder(1);
            Products = _itemService.GetItems();
            Cencel = new DelegateCommand(() => Items1 = new ObservableCollection<Item>());
            ListView1 = new ObservableCollection<Item>();
            foreach (var allItem in MyDataSource.AllItems)
            {
                if (allItem.Type == ProductType.Purchase)
                {
                    ListView1.Add(allItem);

                }
            }


            TotalValue = 0;
            TotalItems = 0;
            Items1 = new ObservableCollection<Item>();

            Checkout = new DelegateCommand(CheckoutInvoice);
        }

        private void CheckoutInvoice()
        {

            var master = new InvoiceMaster
            {
                InvoiceType = InvoiceType.SI,
                Dated = DateTime.Now,
                DateCreated = DateTime.Now
            };
            List<InvoiceDetail> detail = new List<InvoiceDetail>();
            foreach (var item in Items1)
            {
                var itemed = _itemService.GetItem(item.Id);
//                detail.Add(new InvoiceDetail() { Item = itemed, InvoiceMaster = master, Dated = DateTime.Now, CreatedBy = MyDataSource.LoggedInUser.Id });
            }
            master.Amount = TotalValue;


            IDbFactory dbFactory = new DbFactory();
            IInvoiceMasterRepository invoiceMasterRepository = new InvoiceMasterRepository(dbFactory);
            IInvoiceMasterService invoiceMasterService = new InvoiceMasterService(new UnitOfWork(dbFactory), invoiceMasterRepository);
            _invoiceDetailService.AddRange(detail);
            _invoiceDetailService.SaveInvoiceDetail();

            invoiceMasterService.CreateInvoiceMaster(master);
            _invoiceMasterService.SaveInvoiceMaster();
            foreach (var item in Items1)
            {
                var item1 = _itemService.GetItem(item.Id);
                item1.Qty = item1.Qty + item.Qty;
                _itemService.UpdateItem(item1);
            }
            Items1 = null;
            Items1 = new ObservableCollection<Item>();
            TotalValue = 0;
            TotalItems = 0;
        }

        public string Categor3 { get; set; }



        public ObservableCollection<Item> ListView1
        {
            get { return _listView1; }
            set { SetProperty(ref _listView1, value); }
        }




        //        public InvoiceDetail InvoiceDetail
        //        {
        //            get { return _invoiceDetail; }
        //            set
        //            {
        //              SetProperty(ref _invoiceDetail, value);
        //                if (value.Amount > 0)
        //                {
        //             TotalList.Add(value);
        //
        //                }
        //            }
        //        }
        public ObservableCollection<Item> Items1 { get { return _items; } set { SetProperty(ref _items, value); } }
        private ObservableCollection<Item> _listView2;
        private double totalValue;
        private int _totalItems;

        public Item SelectedItems
        {
            get { return _selectedItems; }
            set
            {

                //  SetProperty(ref _selectedItems, value);
                if (value != null)
                {

                    TotalValue = TotalValue + value.Price;
                    TotalItems++;
                    if (Items1.Contains(value) && Items1.Count > 0)
                    {

                        Item item = Items1.First(item1 => item1 == value);
                        var v = _itemService.GetItem(value.Id).Qty;
                        if (Items1.First(item1 => item1 == value).Qty + 1 <= _itemService.GetItem(value.Id).Qty)
                        {
                            item.Qty = item.Qty + 1;

                            Items1.Remove(item);

                            item.Price = _itemService.GetItem(value.Id).Price;
                            item.Discount = _itemService.GetItem(value.Id).Discount;
                         
                            Items1.Add(item);
                            Items1.Move(Items1.Count - 1, 0);
                        }

                    }
                    else
                    {
                        value.Qty = 1;
                        value.Discount = _itemService.GetItem(value.Id).Discount;
                      
                        Items1.Add(value);
                        Items1.Move(Items1.Count - 1, 0);
                    }

                }
            }
        }

        private int _qty;
        private string _vendorName;
        public DelegateCommand Checkout { get; private set; }

        public double TotalValue
        {
            get { return totalValue; }
            set { SetProperty(ref totalValue, value); }
        }

        public int qty
        {
            get { return _qty; }
            set
            {
                SetProperty(ref _qty, value);

            }
        }

        public int TotalItems
        {
            get { return _totalItems; }
            set { SetProperty(ref _totalItems, value); }
        }
    }
    }

