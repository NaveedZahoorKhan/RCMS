﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Windows.Threading;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using Prism.Regions;
using RCMS.App.Resources;
using RCMS.Commons;
using RCMS.Commons.HelperClasses;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Services;
using RCMS.Services.Interfaces;

namespace RCMS.App.ViewModels
{
    public class HomeViewModel : BindableBase
    {

        #region Fields
        private string _title = "Retail Chain Management System";
        private string _profilepic = @"../Resources/icons/profile-default-male.png";
        private string _designation = "Developer";
        private string _name = "Enn Zee";
        private string _myclock;
        private bool _ringDisplay = false;
        private long _billingbadge;
        private long _pruchasebadge;
        private readonly IDbFactory _factory;



        #endregion

        #region Properties



        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public string ProfilePic
        {
            get { return _profilepic; }
            set { SetProperty(ref _profilepic, value); }
        }

        public string Designation
        {
            get { return _designation; }
            set { SetProperty(ref _designation, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }
        public string MyClock { get { return (string)_myclock; } set { SetProperty(ref _myclock, value); } }
        public DelegateCommand SideBar { get; set; }

        public bool RingDisplay
        {
            get { return _ringDisplay; }
            set { SetProperty(ref _ringDisplay, value); }
        }
        
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }
        public DelegateCommand<string> Billing { get; private set; }
        public DelegateCommand<string> Management { get; private set; }
        public DelegateCommand<string> Reports { get; private set; }
        public DelegateCommand<string> Sales { get; private set; }
        public Navigation Navigation { get; set; }
        public long BillingBadge { get { return _billingbadge; } set { SetProperty(ref _billingbadge, value); } }
        public long PurchaseBadge { get { return _pruchasebadge; } set { SetProperty(ref _pruchasebadge, value); } }
        long _stocksBadge;

        public long StocksBadge
        {
            get { return _stocksBadge; }
            set
            {
                SetProperty(ref _stocksBadge, value);
            }
        }

        #endregion



        public HomeViewModel(IRegionManager regionManager, IDbFactory factory, IInvoiceMasterService invoiceMasterService, IItemService itemService, ICategoryService categoryService)
        {


            #region Navigation

            Navigation = new Navigation(regionManager);
            Billing = new DelegateCommand<string>(Navigator);
            Reports = new DelegateCommand<string>(Navigator);
            Management = new DelegateCommand<string>(Navigator);
            Sales = new DelegateCommand<string>(Navigator);

            #endregion

            #region Badges
            BillingBadge = invoiceMasterService.Count(master => master.Dated == DateTime.Now && master.InvoiceType == InvoiceType.SI);
            PurchaseBadge = invoiceMasterService.Count(master => master.Dated == DateTime.Now && master.InvoiceType == InvoiceType.PI);



            #endregion

            #region DashboardMetaData

            Name = MyDataSource.LoggedInUser.Name;
            Designation = MyDataSource.LoggedInUser.Designation;
            ProfilePic = MyDataSource.LoggedInUser.UserImg;


            #endregion

            #region Clock

            DispatcherTimer dispatcherTimer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal,
                delegate
                {
                    MyClock = DateTime.Now.ToString("HH:mm:ss");
                }, Dispatcher.CurrentDispatcher);




            #endregion



            this._factory = factory;


            //StocksBadge = itemService.GetItemCount();
            //            SideBar = new DelegateCommand(() => );

            #region Chart
            PointLabel = chartPoint =>
                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);



            #endregion
        }
        public Func<ChartPoint, string> PointLabel { get; set; }


        #region Methods

        private void Navigator(string path)
        {
            RingDisplay = true;
            Navigation.Navigate(path);
        }


        #endregion
    }
}
