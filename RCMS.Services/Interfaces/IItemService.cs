﻿using System.Collections.Generic;
using RCMS.Models;

namespace RCMS.Services.Interfaces
{
    public interface IItemService : IService<Item>
    {

        IEnumerable<Item> GetItems();
        IEnumerable<Item> GetItemsWithoutUnits();
        long GetItemCount();
        Item GetItem(int id);
        void CreateItem(Item Item);
        void DeleteItem(Item item);
        void UpdateItem(Item Item);
        void SaveItem();
        void RefreshEntity(Item Item);
        IEnumerable<Item> GetItemsByCategory(Category category);
    }
}
