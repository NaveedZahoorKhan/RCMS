﻿namespace RCMS.Models
{
    public class Vendor : ModelBase
    {
        public string VendorCode { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
       public string Cell { get; set; }
        public virtual User UserId { get; set; }
    }
}
