﻿using System;

namespace RCMS.Models
{
    public class Expense : ModelBase
    {
        public DateTime Dated { get; set; }
        public int Amount { get; set; }
        public int Remarks { get; set; }
        

        public virtual ExpenseType ExpenseType { get; set; }
    }
}
