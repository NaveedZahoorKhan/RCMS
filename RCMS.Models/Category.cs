﻿using System.ComponentModel.DataAnnotations;

namespace RCMS.Models
{
    public class Category : ModelBase
    {
        [MaxLength(40)]
        public string  Name { get; set; }
        public string Description { get; set; }
        public string ItemLowerLimit { get; set; }
        public string CategoryImage { get; set; }
        public int CategoryOrder { get; set; }
        
    }
}
