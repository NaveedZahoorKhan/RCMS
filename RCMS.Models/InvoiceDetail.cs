﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RCMS.Models
{
   public class InvoiceDetail : ModelBase
    {
       
        public DateTime Dated { get; set; }
        public string Remarks { get; set; }
        public int Qty { get; set; }
        public double Price { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public double Total => Qty * Price;

        // Foreign Key
        public virtual Item Item { get; set; }

       

    }
}
