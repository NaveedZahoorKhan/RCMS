﻿using System;
using System.Collections.Generic;
using RCMS.Commons;

namespace RCMS.Models
{
    public class InvoiceMaster : ModelBase
    {
       public DateTime Dated { get; set; }
        public InvoiceType InvoiceType { get; set; } 
        public string CustomerName { get; set; }

        public double Amount { get; set; }


        // foreign key
        public virtual List<InvoiceDetail> InvoiceDetails { get; set; }
        



    }
}
