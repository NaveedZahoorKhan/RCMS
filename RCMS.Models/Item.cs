﻿using System.ComponentModel.DataAnnotations;
using RCMS.Commons;

namespace RCMS.Models
{
    public class Item : ModelBase
    {
        [MaxLength(40)]
        public string Name { get; set; }
        public string Code { get; set; }
        public string Img { get; set; }
       
        public string Discription { get; set; }
        public ProductType Type { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }


        public double TotalPrice => Price - Discount;

        public int Qty { get; set; }
        // Foreign Keys
       public virtual ProductUnit ProductUnit { get; set; }
       public virtual Category Category { get; set; }

      
    }
}
