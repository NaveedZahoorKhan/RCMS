﻿using System.ComponentModel.DataAnnotations;

namespace RCMS.Models
{
    public class ExpenseType : ModelBase
    {
        [Required]
        public string Type { get; set; }
        public string Detail { get; set; }

    }
}
