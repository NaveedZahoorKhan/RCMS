﻿using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.Models;

namespace RCMS.DAL.Repositories.Interfaces
{
    public interface IInvoiceDetailRepository : IRepository<InvoiceDetail>
    {
    }
}