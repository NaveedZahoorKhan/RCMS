﻿

using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.Models;

namespace RCMS.DAL.Repositories.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}