﻿using System.Collections.Generic;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.Models;

namespace RCMS.DAL.Repositories.Interfaces
{
    public interface IItemRepository : IRepository<Item>
    {
        IEnumerable<Item> GetItemsByCategory(Category category);
        long GetItemCount();
    }
}
