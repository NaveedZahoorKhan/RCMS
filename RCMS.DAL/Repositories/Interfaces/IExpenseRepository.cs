﻿using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.Models;

namespace RCMS.DAL.Repositories.Interfaces
{
    public interface IExpenseRepository : IRepository<Expense>
    {
    }
}
