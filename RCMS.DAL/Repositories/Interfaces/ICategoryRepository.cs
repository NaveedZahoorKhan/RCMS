﻿
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.Models;

namespace RCMS.DAL.Repositories.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Category GetCategoryByCategoryOrder(int order);
    }
}