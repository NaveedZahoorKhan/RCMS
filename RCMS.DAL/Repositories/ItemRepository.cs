﻿using System.Collections.Generic;
using System.Linq;
using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;

namespace RCMS.DAL.Repositories
{
    public class ItemRepository : RepositoryBase<Item> , IItemRepository
    {
        public ItemRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Item> GetItemsByCategory(Category category)
        {
            return GetMany(item => item.Category.CategoryOrder == category.CategoryOrder);
        }

        public long GetItemCount()
        {
            var v =  DbContext.Items.SqlQuery("select sum(price) from dbo.Products").ToList();
            
            return 0;
        }
        
    }
}
