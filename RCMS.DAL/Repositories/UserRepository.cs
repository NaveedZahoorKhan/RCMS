﻿using RCMS.DAL.Infrastructure;
using RCMS.DAL.Infrastructure.Interfaces;
using RCMS.DAL.Repositories.Interfaces;
using RCMS.Models;

namespace RCMS.DAL.Repositories
{
    public class UserRepository : RepositoryBase<User> , IUserRepository
    {
        public UserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }


        public User UserLogin(User userToLogin)
        {
            var user = GetSingle(m => m.Name == userToLogin.Name && m.Password == userToLogin.Password);
            if (user == null)
            {
                return user;
            }
            else if (user.Password == userToLogin.Name && user.Password == userToLogin.Password)
            {

                return user;

            }
            return user;
        }
    }
}
