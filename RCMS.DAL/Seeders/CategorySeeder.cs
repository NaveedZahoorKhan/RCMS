﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCMS.Models;

namespace RCMS.DAL.Seeders
{
    public struct CategorySeeder
    {
       public static List<int> CategoryIds = new List<int> {1, 2, 3, 4, 5};

        public static List<Category> GetCategories()
        {
            return CategoryIds.Select(i => new Category()
            {
                CategoryOrder = i,
                Name = "Category"+ $"{i}".PadRight(3),
                DateCreated = DateTime.Now,
                Id = i,
                ItemLowerLimit = "20",
            }).ToList();
        }
        
    }
}