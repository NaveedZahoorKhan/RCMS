﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCMS.Models;
using RCMS.Commons;

namespace RCMS.DAL.Seeders
{
    public struct InvoiceMasterSeeder
    {
        public static IEnumerable<int> InvoiceMasterIds = Enumerable.Range(0,10);
        public static List<double> Prices = new List<double> {500.40, 42.12, 32.22, 44.22, 433.0, 555, 12, 32.23, 312.23, 323, 223.2, 323, 22, 12};
        public static Random Random = new Random();
        public static List<InvoiceMaster> GetInvoiceMasters()
        {
            return InvoiceMasterIds.Select(i => new InvoiceMaster()
            {
                Id = i,
                DateCreated = DateTime.Now,
                Amount = Prices[i],
                Dated = new DateTime(2000, 1, 1).AddHours(Random.Next(1000, 2000)),
                InvoiceType = InvoiceType.PI,
                
                
            }).ToList();
        }
    }
}