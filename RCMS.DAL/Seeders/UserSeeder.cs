﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCMS.Models;

namespace RCMS.DAL.Seeders
{
    public struct UserSeeder
    {
        public static List<int> UserIDs = new List<int> {1, 2, 3};

        public static List<User> GetInitialUsers()
        {
            return UserIDs.Select(i => new User()
            {
                Id = UserIDs[i-1],
                Name = "User "+$"{i}".PadLeft(3, '0'),
                Addresses = new Addresses() { AddressLine1 = "XYZ Address", City = "Lahore", Country = "Pakistan", DateCreated = DateTime.Now},
                Designation = "Dummy",
                IsActive = true,
                Password = "1234",
                
            }).ToList();
        }

    }
}