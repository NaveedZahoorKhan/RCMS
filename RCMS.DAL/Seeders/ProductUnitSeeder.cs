﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCMS.Models;

namespace RCMS.DAL.Seeders
{
    public struct ProductUnitSeeder
    {
      public static List<int> ProductUnitIds = new List<int>() {1,2,3,4,5};

        public static List<ProductUnit> GetProductUnits()
        {
            return ProductUnitIds.Select(i => new ProductUnit()
            {
              Id  = i,
              Unit = "ProductUnit"+ $"{i}".PadRight(2),
              UnitFullName = "ProductUnitFullName"+ $"{i}".PadRight(2),
              DateCreated = DateTime.Now,
            }).ToList();
        }
    }
}