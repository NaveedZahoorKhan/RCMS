﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCMS.Models;

namespace RCMS.DAL.Seeders
{
    public struct VendorSeeder
    {
        public static List<int> VendorsIds = new List<int> {1,2,3,4,5};
        private static Random _random = new Random();
        public static List<Vendor> GetVendors()
        {
            return VendorsIds.Select(i => new Vendor()
            {
                Id = i,
                Cell =  new string(Enumerable.Repeat("123456789", 11).Select(s => s[_random.Next(s.Length)]).ToArray()),
                Phone =  new string(Enumerable.Repeat("123456789", 11).Select(s => s[_random.Next(s.Length)]).ToArray()),
                CompanyName = new string(Enumerable.Repeat("abcdefghijklmnopqrstuvwxyz", 11).Select(s => s[_random.Next(s.Length)]).ToArray()),
                UserId =  UserSeeder.GetInitialUsers()[_random.Next(1, 3)],
                DateCreated = DateTime.Now,
                VendorCode = new string(Enumerable.Repeat("123456789abcdefghijklmnopqrstuvwxyz", 11).Select(s => s[_random.Next(s.Length)]).ToArray()),
            }).ToList();
        }
    }
}