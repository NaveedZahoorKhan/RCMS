﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCMS.Models;

namespace RCMS.DAL.Seeders
{
    public struct InvoiceDetailSeeder
    {
        public static IEnumerable<int> InvoiceDetailIDs = Enumerable.Range(0, 10);
        public static Random Random= new Random();
        public static List<InvoiceDetail> GetInvoiceDetails()
        {
            return InvoiceDetailIDs.Select(i => new InvoiceDetail()
            {
                DateCreated = DateTime.Now,
                Id = i,
               
                Remarks = "Purchase Invoice",
                Dated = new DateTime(2000, 1, 1).AddHours(Random.Next(1000, 2000))
            }).ToList();
        }
    }
}