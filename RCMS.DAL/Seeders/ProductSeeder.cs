﻿using System;
using System.Collections.Generic;
using System.Linq;
using RCMS.Models;

namespace RCMS.DAL.Seeders
{
    public struct ProductSeeder
    {
        public static IEnumerable<int> ProductIds = Enumerable.Range(11, 20);
        public static Random Random = new Random();
        public static List<Item> GetProducts()
        {
            return ProductIds.Select(i => new Item()
            {
                Category = new Category()
                {
                    CategoryOrder = i-10,
                    Name = "Category" + $"{i}".PadRight(3),
                    DateCreated = DateTime.Now,
                    Id = i-10,
                    ItemLowerLimit = "20",
                },
                Price = Random.NextDouble()  * 100,
                Qty = Random.Next(0, 50),
                Code = "XXXX-"+$"{i}".PadRight(4),
                DateCreated = DateTime.Now,
                Id = i,
                Name = new string(Enumerable.Repeat("abcdefghijklmnopqrstuvwxyz", 11).Select(s => s[Random.Next(s.Length)]).ToArray()).ToUpper(),
                Discription = "This is a product discription",
                ProductUnit = ProductUnitSeeder.GetProductUnits()[Random.Next(0,5)],
                Discount = Random.Next(0, 20),
                

            }).ToList();
        }
    }
}